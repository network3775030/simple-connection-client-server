import socket

def main():
    host = '127.0.0.1'
    port = 12345

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    server_socket.listen(1)
    print("Server listening on {}:{}".format(host, port))

    conn, addr = server_socket.accept()
    print("Connection established with {}:{}".format(addr[0], addr[1]))

    for num in range(1, 100):
        conn.send(str(num).encode())
        data = conn.recv(1024).decode()
        print("Received:", data)
        num = int(data) + 1

    conn.close()

if __name__ == "__main__":
    main()
